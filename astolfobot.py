from typing import Tuple
from maubot import Plugin, MessageEvent
from maubot.handlers import command

class astolfobot(Plugin):
    @command.passive("owo", case_insensitive=True)
    async def owo(self, evt: MessageEvent, match: Tuple[str]) -> None:
        await evt.reply("UwU?")
    async def uwu(self, evt: MessageEvent, match: Tuple[str]) -> None:
        await evt.reply("OwO?")
    @command.passive("Poland", case_insensitive=True)
    @command.passive("polish", case_insensitive=True)
    async def poland(self, evt: MessageEvent, match: Tuple[str]) -> None:
        await evt.respond("https://media.discordapp.net/attachments/899258099968405517/938544080290480188/3942866d-81fc-4d7c-b453-989c5b2b52cc.png")
    @command.passive("sad", case_insensitive=True)
    async def sadCyprus(self, evt: MessageEvent, match: Tuple[str]) -> None:
        await evt.reply("cyprus moment")
